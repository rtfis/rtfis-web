<div>
    <figure class="logo">&nbsp;</figure>
    <p class="feature">Registration &#9679; Get Recommending Dish &#9679; Get Recommending Ingredient</p>
    <p class="subject">What are you looking for your cooking</p>
    <form class="form">
        <input title="Search" placeholder="&#127859; Ex.Tomyumkung" type="text">
        <select id="search-type" title="Search Type">
        </select>
        <button title="GO" onclick='openModal("components/search.php","Searching")'>GO</button>
    </form>
    <div class="account">
        <button onclick='openModal("components/register.php","Registration")'>REGISTER</button>
        &#9679;
        <button onclick='openModal("components/login.php","Authentication")'>LOGIN</button>
    </div>
</div>

<script>
    let searchTypeSelect = document.querySelector('#search-type');
    function homeInitial() {
        fetch(searchTypeUrl)
            .then(res => res.json())
            .then(data => {
                // iterate over users
                data.map((data) => {
                    // create the elements
                    let option = createNode('option');
                    option.value = data.searchTypeId;
                    option.innerText = data.searchTypeName;
                    // append all elements
                    appendNode(searchTypeSelect, option);
                });
            }).catch(err => {
            console.error('Error: ', err);
        });
        console.log("HOME INITIAL");
    }

    homeInitial();
</script>

