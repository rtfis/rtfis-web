<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Recommending Thai Food Ingredient Substitutions</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="author" content="Peerapat Pitak"/>
    <meta name="description" content="RECOMMENDING THAI FOOD INGREDIENT SUBSTITUTIONS"/>
    <meta name="keywords" content="RECOMMENDING THAI FOOD INGREDIENT SUBSTITUTIONS"/>
    <meta name="Resource-type" content="Document"/>

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>

    <link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-4.4.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/vendors/fontawesome-5.13.0/css/all.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/vendors/fullpage-3.0.8/css/fullpage.css"/>
    <link rel="stylesheet" type="text/css" href="assets/vendors/toastr-2.1.4-7/css/toastr.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/vendors/boostrap-fileinput-4.3.7/css/fileinput.min.css"/>

    <link rel="stylesheet" type="text/css" href="assets/styles/commons.css"/>
    <link rel="stylesheet" type="text/css" href="assets/styles/fullpage-custom.css"/>
    <link rel="stylesheet" type="text/css" href="assets/styles/page-home.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/page-main-features.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/contact-us.css">
    <script type="text/javascript" src="assets/vendors/jquery-3.5.0/js/jquery-3.5.0.min.js"></script>
    <script type="text/javascript" src="assets/scripts/common.js"></script>
</head>
<body>
<div id="header">
    <figure id="logo-pic">&nbsp;</figure>
    <p id="logo-text">Recommending Thai Food Ingredient Substitutions</p>
    <ul id="menu">
        <li data-menuanchor="home"><a href="#home">Home</a></li>
        <li data-menuanchor="mainFeatures"><a href="#mainFeatures">Main Features</a></li>
        <li data-menuanchor="thaiDish"><a href="#thaiDish">Thai Dish</a></li>
        <li data-menuanchor="contactUs"><a href="#contactUs">Contact Us</a></li>
    </ul>
</div>

<?php include 'components/modal.php'; ?>
<div id="fullpage">
    <div class="section home">
        <!-- Use any element to open/show the overlay navigation menu -->
        <?php include 'pages/home.php'; ?>
    </div>
    <div class="section main-features">
        <?php include 'pages/main-features.php'; ?>
    </div>
    <div class="section thai-dish">
        <?php include 'pages/thai-dish.php'; ?>
    </div>
    <div class="section contact-us">
        <?php include 'pages/contact-us.php'; ?>
    </div>
</div>
<script type="text/javascript" src="assets/vendors/bootstrap-4.4.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="assets/vendors/fullpage-3.0.8/js/fullpage.js"></script>
<script type="text/javascript" src="assets/vendors/fontawesome-5.13.0/js/all.min.js"></script>
<script type="text/javascript" src="assets/vendors/toastr-2.1.4-7/js/toastr.min.js"></script>
<script type="text/javascript" src="assets/vendors/boostrap-fileinput-4.3.7/js/fileinput.min.js"></script>

<script type="text/javascript" src="assets/scripts/fullpage-custom.js"></script>
</body>
</html>