<form accept-charset="UTF-8" enctype="multipart/form-data">
    <hr class="modal_action_line">
    <div class="form-group">
        <input id="user-pic" type="file" name="userPic" required>
        <input type="hidden" name="userFileGroup" value="pic-user">
    </div>
    <div class="form-group">
        <label for="register-email">Email Address</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text">@</div>
            </div>
            <input id="register-email" type="email" class="form-control" placeholder="name@example.com" name="userEmail"
                   required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="name">Name</label>
            <input id="name" type="text" class="form-control" placeholder="Name" name="userName" required>
        </div>
        <div class="form-group col-md-6">
            <label for="login-last-name">Last Name</label>
            <input id="login-last-name" type="text" class="form-control" placeholder="Last Name" name="userLastName"
                   required>
        </div>

    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="password">Password</label>
            <input id="password" type="password" class="form-control" placeholder="Password" name="userPassword"
                   required>
        </div>
        <div class="form-group col-md-6">
            <label for="re-password">Re Password</label>
            <input id="re-password" type="password" class="form-control" placeholder="Re-Password" required>
        </div>
    </div>
    <hr class="modal_action_line">
    <button class="btn modal_action_btn" onclick="userRegisterSubmit(this.form)" type="button">REGISTER</button>
<!--    <button class="btn modal_action_btn"  type="submit">REGISTER</button>-->
</form>
<script>
    function userRegisterSubmit(elm) {
        const url = 'http://localhost:9999/rtfis-web-service/user/register';
        let data = new FormData(elm);//getFormData($(elm));
        fetch(url, {
            method: 'POST',
            body: data,
        }).then(response => response.text())
            .then(data => {
                if ("SUCCESS" == data) {
                    toastr.success("Register Success");
                } else {
                    toastr.error("Register Fail");
                }
            })
            .finally(function () {
                closeModal();
            });
    }

    // $('#film').submit(function(event) {
    //     var formElement = this;
    //     // You can directly create form data from the form element
    //     // (Or you could get the files from input element and append them to FormData as we did in vanilla javascript)
    //     var formData = new FormData(formElement);
    //     formData.append( 'userPic', $( '#user-pic' )[0]);
    //
    //     $.ajax({
    //         type: "POST",
    //         enctype: 'multipart/form-data',
    //         url: "http://localhost:9999/user/register",
    //         data: formData,
    //         processData: false,
    //         contentType: false,
    //         success: function (response) {
    //             console.log(response);
    //             // process response
    //         },
    //         error: function (error) {
    //             console.error(error);
    //             // process error
    //         }
    //     });
    //
    //     event.preventDefault();
    // });
</script>