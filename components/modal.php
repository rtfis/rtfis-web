<style>
    #modal-subject{
        font-family: logoFont;
        margin: auto;
        width: 100%;
        text-align: center;
        display: block;
        font-size: 3vw;
        color: #fff;
        text-decoration: underline;
    }
    #modal-content{
        padding: 0 10vw;
        color: white;
        font-size: 1.5vw;
    }
    .modal_action_line{
        border: solid 1px #fff;
        margin: 2em 0em;
    }
    .modal_action_btn{
        background: #a20100;
        font-size: 1.5vw;
        padding: 0.5em 1.5em;
        color: #fff;
        display: block;
        margin: auto;
        font-weight: bold;
    }
    .modal_action_btn:hover{
        color: gold;
    }
    /* The overlay effect with black background */
    .overlay {
        height: 100%;
        width: 100%;
        display: none;
        position: fixed;
        z-index: 1;
        top: 37px;
        left: 0;
        background-color: rgb(0, 0, 0);
        background-color: rgba(0, 0, 0, 0.9); /* Black with a little bit see-through */
    }

    /* Close button */
    .overlay .closebtn {
        font-size: 4vw;
        cursor: pointer;
        color: #a20100;
        margin: auto;
        width: 100%;
        text-align: center;
        display: block;
    }

    .overlay .closebtn:hover {
        color: #ffd700;
        text-decoration: none;
    }

</style>
<div id="modalWindow" class="overlay">
    <a href="javascript:void(0)" class="closebtn" onclick="closeModal()">&times;</a>
    <h id="modal-subject"></h>
    <div id="modal-content"></div>
</div>

<script>
    const modalWindow = $('#modalWindow');
    const modalSubject = $('#modal-subject');
    const modalContent = $('#modal-content');
    function openModal(elm,subject) {
        modalWindow.show();
        $.get(elm, function (data) {
            modalSubject.text(subject);
            modalContent.html(data);
        });
    }
    function closeModal() {
        modalContent.html("");
        modalWindow.hide();
    }
</script>