<form accept-charset="UTF-8" enctype="multipart/form-data">
    <input id="dish-pic" type="file" name="dishPic" required>
    <input type="hidden" name="dishFileGroup" value="pic-dish">
    <input type="text" name="dishName">
    <textarea name="dishDescription"></textarea>
    <select id="dish-type" name="dishTypeId" title="Dish Type">
    </select>
    <button class="btn modal_action_btn" onclick="addDishSubmit(this.form)" type="button">ADD</button>
</form>
<script>
    var dishTypeSelect = document.querySelector('#dish-type');
    function addDishInitial() {
        fetch(dishTypeUrl)
            .then(res => res.json())
            .then(data => {
                // iterate over users
                data.map((data) => {
                    // create the elements
                    let option = createNode('option');
                    option.value = data.dishTypeId;
                    option.innerText = data.dishTypeName;
                    // append all elements
                    appendNode(dishTypeSelect, option);
                });
            }).catch(err => {
            console.error('Error: ', err);
        });
        console.log("ADD DISH INITIAL");
    }
    addDishInitial();

    function addDishSubmit(elm) {
        const url = 'http://localhost:9999/dish/add';
        let data = new FormData(elm);
        fetch(url, {
            method: 'POST',
            body: data,
        }).then(response => response.text())
            .then(data => {
                if ("SUCCESS" == data) {
                    toastr.success("Add Dish Success");
                } else {
                    toastr.error("Add Dish Fail");
                }
            })
            .finally(function () {
                closeModal();
            });
    }
</script>
