$(document).ready(function () {
    acknowledge();
});


// create an element
const createNode = (elem) => {
    return document.createElement(elem);
};

// append an element to parent
const appendNode = (parent, elem) => {
    parent.appendChild(elem);
}

function acknowledge() {
    fetch(acknowledgeUrl)
        .then(res => res.text())
        .then(data => {
            if ("SUCCESS" === data) {

            } else {
                alert("ERROR");
            }
        }).catch(err => {
        console.error('Error: ', err);
        alert("ERROR")
    });
}

function getFormData($form) {
    let unindexed_array = $form.serializeArray();
    let indexed_array = {};
    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}