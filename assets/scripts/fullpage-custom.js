var fullpage = new fullpage('#fullpage', {
    anchors: ['home', 'mainFeatures','thaiDish','contactUs'],
    sectionsColor: ['', '#FDF2E9','#FDF2E9','#FDF2E9'],
    navigation: true,
    navigationPosition: 'right',
    //navigationTooltips: ['First page', 'Second page', 'Third and last page', ''],
    responsiveWidth: 900,
    menu: '#menu',
    afterResponsive: function (isResponsive) {

    }
});